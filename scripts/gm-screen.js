class GmScreen {

  // Call hooks in the constructor.
  constructor() {
    Hooks.once('init', this.init.bind(this));
    Hooks.on('getSceneControlButtons', this.getSceneControlButtons.bind(this));
    Hooks.on('renderSceneControls', this.renderSceneControls.bind(this));
    Hooks.on('renderDialog', this.renderDialog.bind(this));
    Hooks.on('renderJournalSheet', this.renderJournalSheet.bind(this));
  }

  /**
   * Implements Hooks.on('init')
   */
  init() {
    // Register a hidden setting to store the stringified version of the
    // GM Screen contents.
    game.settings.register('gm-screen', 'screenContents', {
      name: 'GM Screen Contents',
      hint: 'Storage for GM screen contents (journal entries).',
      scope: 'world',
      config: false,
      default: '[]',
      type: String,
    });
  }

  /**
   * Implements Hooks.on('canvasInit')
   */
  getSceneControlButtons(controls) {
    // Add a new control for the GM screen. Note that this is a bit hacky;
    // we need to hook into the controls render hook and unbind the core
    // event for it to prevent errors.
    let control = {
      name: 'gm-screen',
      title: 'CONTROLS.GmScreen',
      layer: 'GmScreenLayer',
      icon: 'fas fa-columns',
      visible: game.user.isGM,
      tools: [
        {
          name: 'select',
          title: 'Open Screen',
          icon: 'fas fa-expand'
        }
      ]
    };
    controls.push(control);
  }

  /**
   * Implements Hooks.on('renderSceneControls')
   */
  renderSceneControls() {
    // Retrieive the GM Screen control, unbind the core event handler, and then
    // bind our own handler to open and close the GM Screen.
    let $control = $('.scene-control[data-control="gm-screen"]');
    $control.unbind().bind('click', ev => {
      let $dialog = $('.gm-screen-dialog');
      if ($dialog.length < 1) {
        this.renderScreen();
      }
      else {
        $dialog.remove();
      }
    });
  }

  /**
   * Add draggable listeners.
   *
   * @param {Object} selector
   *   jQuery object (optional).
   * @param {Boolean} deleteItem
   *   Whether or not to clear the matching item from the GM Screen. Requires
   *   `selector` to have page, row, and col data attributes.
   */
  addJournalListener(selector = null, deleteItem = false) {
    let $journal = selector;
    // Add journal ID to journal entries in the sidebar.
    if (selector === null) {
      $journal = $('.journal.directory-item');
    }
    $journal.attr('draggable', 'true');
    $journal.on('dragstart', ev => {
      let $self = $(ev.originalEvent.target);
      ev.originalEvent.dataTransfer.setData('gm-screen-id', $self.data('entity-id'));

      if (deleteItem) {
        let position = `${$self.attr('data-page')}|${$self.attr('data-row')}|${$self.attr('data-col')}`;
        ev.originalEvent.dataTransfer.setData('gm-screen-clear', position);
      }
    });
  }

  /**
   * Implements Hooks.on('renderJournalSheet')
   *
   * Editing existing journal entries or creating new ones replaces the journal
   * sidebar contents. Update the entries to have new event listeners to
   * work around that.
   */
  renderJournalSheet() {
    this.addJournalListener();
  }

  /**
   * Implements Hooks.on('renderDialog')
   *
   * This is the hook where the majority of our logic fires, as it's triggered
   * every time the GM screen is opened.
   */
  renderDialog(dialog) {
    // If this isn't a GM screen dialog, exit.
    if (!dialog._element.hasClass('gm-screen-dialog')) {
      return;
    }

    // Add journal ID to journal entries in the sidebar.
    this.addJournalListener();

    // Add journal ID listener to GM Screen grid.
    this.addJournalListener($('.gm-screen-grid .grid-item'), true);

    // Handle the pager.
    let $pages = $('.gm-screen-grid .grid-page');
    $pages.first().addClass('active');
    let $pager = $('.gm-screen-pager');

    $pager.on('click', ev => {
      let $self = $(ev.target);
      let $parent = $self.closest('.gm-screen-pager');
      let $active = $('.gm-screen-grid .grid-page.active');
      let $gridWrapper = $('.gm-screen-grid .grid-wrapper');
      let page = parseInt($gridWrapper.attr('data-page'));
      if ($parent.hasClass('prev')) {
        let $sibling = $active.prev('.grid-page');
        if ($sibling.length > 0) {
          $gridWrapper.attr('data-page', page - 1);
          $active.removeClass('active');
          $sibling.addClass('active');
        }
      }
      else {
        let $sibling = $active.next('.grid-page');
        if ($sibling.length > 0) {
          $gridWrapper.attr('data-page', page + 1);
          $active.removeClass('active');
          $sibling.addClass('active');
        }
      }
    });

    // Handle deleting entries.
    let $deleteItem = $('.gm-screen-grid .controls.delete');
    $deleteItem.on('click', ev => {
      let $self = $(ev.target);
      let $item = $self.closest('.grid-item');
      let page = $item.data('page');
      let row = $item.data('row');
      let col = $item.data('col');

      let screenContents = this.loadScreenArray();
      screenContents[page][row][col] = '';
      game.settings.set('gm-screen', 'screenContents', JSON.stringify(screenContents));
      $item.attr('data-entity-id', '');
      $item.find('.name').html('');
      $item.find('.content').html('');
    });

    // Retrieve the grid items.
    let $gridItems = $('.gm-screen-grid .grid-item');
    // Add class when dragging a journal entry over a zone.
    $gridItems.on('dragover', ev => {
      let $self = $(ev.originalEvent.target);
      let $dropTarget = $self;
      if ($self.hasClass('grid-item') === false) {
        $dropTarget = $self.closest('.grid-item');
      }
      $dropTarget.addClass('drop-hover');
      return false;
    // Remove the class when leaving.
    }).on('dragleave', ev => {
      let $self = $(ev.originalEvent.target);
      let $dropTarget = $self;
      if ($self.hasClass('grid-item') === false) {
        $dropTarget = $self.closest('.grid-item');
      }
      $dropTarget.removeClass('drop-hover');
      return false;
    // Handle dropping the journal entry on a grid item.
    }).on('drop', ev => {
      // Get the journal ID and drop target.
      let journalId = ev.originalEvent.dataTransfer.getData('gm-screen-id');
      let $self = $(ev.originalEvent.target);
      let $dropTarget = $self;
      if ($self.hasClass('grid-item') === false) {
        $dropTarget = $self.closest('.grid-item');
      }

      $dropTarget.removeClass('drop-hover');

      // Get the current contents of the GM Screen.
      let screenContents = this.loadScreenArray();
      // Get the placement from the drop target.
      let page = $dropTarget.data('page');
      let row = $dropTarget.data('row');
      let col = $dropTarget.data('col');
      // Determine if we should remove existing entries.
      let position = ev.originalEvent.dataTransfer.getData('gm-screen-clear');
      if (position.length > 0) {
        let positionToClear = position.split('|');
        screenContents[positionToClear[0]][positionToClear[1]][positionToClear[2]] = '';
        let $item = $(`.gm-screen-grid .grid-item[data-page="${positionToClear[0]}"][data-row="${positionToClear[1]}"][data-col="${positionToClear[2]}"]`);
        $item.attr('data-entity-id', '');
        $item.find('.name').html('');
        $item.find('.content').html('');
      }
      // Update the screen contents and write it back to settings.
      screenContents[page][row][col] = journalId;
      game.settings.set('gm-screen', 'screenContents', JSON.stringify(screenContents));
      // Update markup to reflect the new change.
      let journal = game.journal.get(journalId);
      $dropTarget.attr('data-entity-id', journalId);
      $dropTarget.find('.name').html(journal.data.name);
      $dropTarget.find('.content').html(journal.data.content);

      this.addJournalListener($dropTarget, true);

      return false;
    });
  }

  /**
   * Get Journal Contents for the GM Screen template.
   *
   * @param {String} journalId
   * @param {Number} span
   *
   * @return {Object}
   */
  getJournalContents(journalId = null, span = 1) {
    let result = {id: '', name: '', content: '', span: span};
    if (journalId !== null && journalId.length !== undefined && journalId.length > 0) {
      let data = game.journal.get(journalId);
      if (data !== undefined) {
        result = {
          id: journalId,
          name: data.data.name,
          content: data.data.content,
          span: span
        };
      }
    }

    return result;
  }

  /**
   * Retrieve the screen contents as an array.
   */
  loadScreenArray() {
    let journals = JSON.parse(game.settings.get('gm-screen', 'screenContents'));
    // If data is malformed, return a blank version of it.
    if (journals[0] === undefined) {
      let rowSpan = {id: '', name: '', content: '', span: 3};
      let rowSingle = {id: '', name: '', content: '', span: 1};
      let rows = [
        [rowSpan],
        [rowSingle],
        [rowSpan]
      ];
      let pages = [
        rows,
        rows,
        rows,
        rows
      ];
      journals = pages;
    }
    return journals;
  }

  /**
   * Render and return the GM Screen contents as a promise.
   *
   * @param {Array} screenContents
   * @return {Promise}
   */
  async getScreenContent(screenContents = null) {
    // Get the existing screen contents.
    let journals = this.loadScreenArray();
    // If an array was passed in, use that instead.
    if (screenContents !== null) {
      journals = screenContents;
    }
    let pages = [];
    // @TODO: Update to support dynamic pages.
    // For now, assume 4 pages.
    for (let pageIndex = 0; pageIndex < 4; pageIndex++) {
      let page = [];
      // @TODO: Update to support dynamic rows.
      // For now, assume 3 rows.
      for (let rowIndex = 0; rowIndex < 3; rowIndex++) {
        let row = [];
        // @TODO: Update to support dynamic columns.
        // For now, assume 3 columns on the middle row.
        if (rowIndex === 1) {
          for (let colIndex = 0; colIndex < 3; colIndex++) {
            let col = journals[pageIndex][rowIndex][colIndex];
            row.push(this.getJournalContents(col, 1));
          }
        }
        // Other rows are 3 columns wide.
        else {
          let colIndex = 0;
          let col = journals[pageIndex][rowIndex][colIndex];
          row.push(this.getJournalContents(col, 3));
        }
        page.push(row);
      }
      pages.push(page);
    }
    // Prepare and render the template.
    let template = 'modules/gm-screen/templates/grid.hbs';
    let templateData = {
      pages: pages
    };
    return renderTemplate(template, templateData);
  }

  /**
   * Render the GM Screen.
   */
  renderScreen() {
    // Render the GM Screen, and then use it for a new Dialog's contents.
    this.getScreenContent().then(contents => {
      let options = {
        width: 800,
        height: 800,
        classes: ['gm-screen-dialog'],
        resizable: true
      };

      let d = new Dialog({
        title: game.i18n.localize('GMSCREEN.ScreenTitle'),
        content: contents,
        buttons: {}
      }, options);
      d.render(true);
    });
  }

}

// Instantiate the class.
new GmScreen();
