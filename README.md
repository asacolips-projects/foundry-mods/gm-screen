# GM Screen

Create your own customizable GM screen with Journal entries!

# Install

1. Go to the setup page and choose **Add-on Modules**.
2. Click the **Install Module** button, and paste in this manifest link: [https://gitlab.com/asacolips-projects/foundry-mods/gm-screen/raw/master/module.json](https://gitlab.com/asacolips-projects/foundry-mods/gm-screen/raw/master/module.json)

Compatible with FoundryVTT 0.3.x

# How to Use

The GM screen can be activated via the last button in the controls on the left side of the screen if your a GM user, which is an icon that shows two pages side by side.

The GM Screen is made up of four pages, each of which has three rows. The top and bottom rows span the full width of the page, but the middle row has three columns. Rows will attempt to grow in height if they can, but if there's not enough room for their content, they'll have a scroll bar.

To place an entry in the screen, switch to the Journal tab of the right sidebar and drag a Journal Entry from the sidebar into the desired region of the screen. Once an entry has been placed, it can also be moved to other regions by dragging and dropping.

To delete an entry from the screen, hover over the entry and use the Trash icon in the top right corner of the entry. This will not delete the Journal entry, it will only remove it from the GM screen.

[Demo video](https://i.imgur.com/9ZLIm1s.mp4)
